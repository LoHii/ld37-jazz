﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour {

    static GameManager _gm;
    public GameObject player;
    public GameObject tube;

    void Awake() {
        _gm = this;
    }

    public static GameObject GetPlayer() {
        return _gm.player;
    }

    public static GameObject GetTube() {
        return _gm.tube;
    }
}
