﻿using UnityEngine;
using System.Collections;

public class PlayerMover : InputReceiver {

    Rigidbody _rb;
    bool _grounded;
    bool _airCap;
    BoxCollider _collider;
    float _jumpBuffer;
    TubeRotator _tube;
    Transform _model;
    Vector3 _target;
    Animator _anim;
    bool _holdJump;
    [HideInInspector]
    public Quaternion lastRotation;

    public float gravityScale;
    public float jumpSpeed;
    public float groundedAcceleration;
    public float aerialAcceleration;
    public float decceleration;
    public float maxGroundSpeed;
    public float maxAirSpeed;
    public float minSpeed;

	public AudioSource drumJump;
	public RandomSound drumBounce;

    void Awake() {
        _rb = GetComponent<Rigidbody>();
        _collider = GetComponent<BoxCollider>();
        _model = transform.GetChild(0);
        _target = _model.position;
        _anim = GetComponentInChildren<Animator>();

    }

    void Start() {
        GameObject tube = GameManager.GetTube();
        if (tube != null) {
            _tube = tube.GetComponent<TubeRotator>();
        }
    }

    void Update() {
        CheckGrounded();
        _jumpBuffer = Mathf.Max(_jumpBuffer - Time.deltaTime, 0);
        _anim.SetBool("AirTime", !_grounded);
        _anim.SetBool("Walking", _rb.velocity != Vector3.zero);
        _anim.SetBool("Falling", _rb.velocity.y < 0);
    }

    public override void ReceiveInput(InputStruct input) {
		if (MenuManager.IsPaused()) { return; }

        if (input.jump) {
            if (_grounded || _jumpBuffer > 0)
                Jump(jumpSpeed*Vector3.up);
        }

        _holdJump = input.holdJump;

        Move(GetMovementAmount(input.direction));
        RotateModel(input.direction);
    }

    void RotateModel(Vector2 input) {
        _target = new Vector3(_target.x, _model.position.y, _target.z);
        if (_target != _model.position) {
            _model.rotation = Quaternion.RotateTowards(_model.rotation, Quaternion.LookRotation(_target - _model.position), 480 * Time.deltaTime);

            //if ((_target - _model.position).magnitude > 0.1f)
            _target = _model.position;
        }
    }

    void Jump(Vector3 force) {
        _rb.AddForce(force, ForceMode.VelocityChange);
        _anim.SetTrigger("Jump"); // Jump animation
        _grounded = false;
        _jumpBuffer = 0;
    }

    Vector2 GetMovementAmount(Vector2 v) {
        // Normalizes if mag over 1
        if (v.magnitude > 1f) { v = v.normalized; }

        float xAcc, zAcc;

        if (_grounded) {
            xAcc = v.x * groundedAcceleration;
            zAcc = v.y * groundedAcceleration;
        } else {
            xAcc = (Mathf.Sign(v.x) == Mathf.Sign(_rb.velocity.x)) ? v.x * groundedAcceleration : v.x * aerialAcceleration;
            zAcc = (Mathf.Sign(v.y) == Mathf.Sign(_rb.velocity.z)) ? v.y * groundedAcceleration : v.y * aerialAcceleration;
        }

        return new Vector2(xAcc, zAcc);
    }

    void Move(Vector2 acceleration) {
        Vector2 oldVelocity = new Vector3(_rb.velocity.x, 0, _rb.velocity.z);
        _rb.AddForce(new Vector3(acceleration.x, 0, acceleration.y), ForceMode.Acceleration);

        CapSpeed(oldVelocity);
        if (_grounded)
            Deccelerate(acceleration);
    }

    void Deccelerate(Vector2 input) {
        float x = _rb.velocity.x;
        float z = _rb.velocity.z;

        if (input.x == 0) {
            x = Mathf.MoveTowards(x, 0, decceleration * Time.deltaTime);
        }

        if (input.y == 0) {
            z = Mathf.MoveTowards(z, 0, decceleration * Time.deltaTime);
        }

        _rb.velocity = new Vector3(x, _rb.velocity.y, z);
    }

    void CapSpeed(Vector3 oldVelocity) {

        Vector3 newVelocity = new Vector3(_rb.velocity.x, 0, _rb.velocity.z);

        Vector2 cap = new Vector2(maxGroundSpeed, maxGroundSpeed);

        if (_airCap || (!_grounded && oldVelocity.magnitude <= maxAirSpeed)) {
            cap = new Vector2(maxAirSpeed, maxAirSpeed);
            _airCap = true;
        }

        if (newVelocity.magnitude > Mathf.Max(cap.x, cap.y))
            _rb.velocity = new Vector3(newVelocity.normalized.x * cap.x, _rb.velocity.y, newVelocity.normalized.z * cap.y);
    }

    void CheckGrounded() {
        if (Physics.Raycast(transform.position + Vector3.up * 0.01f, Vector3.down, 0.02f)) {
            if (!_grounded)
                lastRotation = GameManager.GetTube().transform.rotation;
            _grounded = true;
            OnLanding();
        } else {
            if (_grounded)
                _jumpBuffer = 0.15f;
            _grounded = false;
        }
    }

    void OnLanding() {
        _airCap = false;
        _anim.SetBool("AirTime", false); // Land animation
        if (_tube != null && gravityScale != 0)
            _tube.ResetCooldown();
    }

    public void Bounce(BounceDrum bd) {
        _rb.velocity = new Vector3(_rb.velocity.x, 0, _rb.velocity.z);
        _airCap = false;
        if (_tube != null && gravityScale != 0)
            _tube.ResetCooldown();

        if (!_holdJump) {
            _rb.velocity = Vector3.zero;
            _rb.AddForce(bd.transform.up * bd.smallBounce, ForceMode.VelocityChange);
            drumBounce.PlaySound(); // Random bounce sound
        } else {
            bd.Break();
            Jump(bd.bigBounce*bd.transform.up);
            drumJump.Play();
		}
    }

    public IEnumerator Rotate(float f) {
        float g = gravityScale;
        float fStart = f;
        gravityScale = 0;
        _anim.SetBool("Jazz", true);
        Quaternion q = _model.rotation;
        Vector3 v = CameraController.GetCamera().transform.position;
        v.Set(v.x, _model.position.y, v.z);
        bool jazz = true;

        while (f > 0) {
            f -= Time.deltaTime;
            if (f > fStart * 0.75f)
                _model.rotation = Quaternion.Lerp(q, Quaternion.LookRotation(_model.position - v), (fStart - f) * 4);

            if (f <= fStart * 0.25f) {
                _model.rotation = Quaternion.Lerp(Quaternion.LookRotation(_model.position - v), q, (fStart * 0.25f - f) * 4);
                if (jazz) {
                    jazz = false;
                    _anim.SetBool("Jazz", false);
                }
            }

            yield return new WaitForEndOfFrame();
        }

        gravityScale = g;
    }

	public bool IsGrounded () {
		return _grounded;
	}
}
