﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MenuManager : MonoBehaviour {

    public float mainMenuFadeTime = 3;
    public GameObject pauseMenu;
    public Text endText;
    public Image end;
    GameObject mainMenu;
    float _quit;

    static MenuManager _mm;
    static bool _paused;
    static bool _menuExit;

    void Start() {
        _mm = this;
        mainMenu = GameObject.Find("MainMenu");
        pauseMenu.SetActive(false);
    }

    void Update() {

        if ((Input.anyKeyDown || Mathf.Abs(Input.GetAxis("Horizontal")) > 0 || Mathf.Abs(Input.GetAxis("Vertical")) > 0) && !_menuExit) {
            StartCoroutine(ExitMenu());
        }

        if (Input.GetButtonDown("Pause")) {
            Pause();
        }

        if (Input.GetButtonUp("Pause")) {
            UnPause();
        }

        if (_paused) {
            _quit += Time.unscaledDeltaTime;

            if (_quit > 3.0f)
                Application.Quit();
        }
    }

    IEnumerator ExitMenu() {
        _menuExit = true;
        Image logo = mainMenu.GetComponentInChildren<Image>();
        Outline logoOutline = logo.GetComponentInChildren<Outline>();
        Text credits = mainMenu.GetComponentInChildren<Text>();

        float fadeTime = mainMenuFadeTime;
        while (fadeTime > 0) {
            var alpha = fadeTime / mainMenuFadeTime;

            var logoColor = logo.color;
            var logoOutlineColor = logoOutline.effectColor;
            var creditsColor = credits.color;

            logoColor.a = alpha;
            logoOutlineColor.a = alpha;
            creditsColor.a = alpha;

            logo.color = logoColor;
            logoOutline.effectColor = logoOutlineColor;
            credits.color = creditsColor;

            fadeTime -= Time.deltaTime;

            yield return new WaitForEndOfFrame();
        }
    }

    public void UnPause() {
        _paused = false;
        pauseMenu.SetActive(false);
        Time.timeScale = 1f;
        _quit = 0;
    }

    void Pause() {
        _paused = true;
        pauseMenu.SetActive(true);
        Time.timeScale = 0;
    }

    public static bool IsPaused() {
        return _paused;
    }

    public static bool MenuExit() {
        return _menuExit;
    }

    public static void End() {
        _mm.StartCoroutine(_mm.EndRoutine());
    }

    IEnumerator EndRoutine() {
        int score = SpawnManager.GetScore();
        int max = SpawnManager.GetMax();

        float fadeTime = 0;
        endText.text = "Congratulations!\n\nYou collected "+score+" out of " + max + " notes!";

        while (fadeTime <mainMenuFadeTime) {
            var alpha = fadeTime / mainMenuFadeTime;

            var endColor = end.color;
            var textColor = endText.color;

            endColor.a = alpha;
            textColor.a = alpha;

            end.color = endColor;
            endText.color =textColor;
            fadeTime += Time.deltaTime;

            yield return new WaitForEndOfFrame();
        }
    }
}
