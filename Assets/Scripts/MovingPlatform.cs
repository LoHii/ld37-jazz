﻿using UnityEngine;
using System.Collections;

public class MovingPlatform : MonoBehaviour {

	public float speed;
	public Transform startPoint, endPoint;

	Rigidbody _rb;
	bool _playerOnPlatform;

	void Start () {
		_rb = GameManager.GetPlayer().GetComponent<Rigidbody>();
		StartCoroutine(MoveToPos(endPoint));
	}

	IEnumerator MoveToPos(Transform t) {
		var dist = Vector3.Distance(transform.position, t.position);

		while(dist > 0) {
			var dir = (t.position - transform.position).normalized;
			transform.position += dir * speed * Time.deltaTime;
			dist -= speed * Time.deltaTime;

			yield return new WaitForEndOfFrame();
		}

		if (t == startPoint) {
			StartCoroutine(MoveToPos(endPoint));
		} else if (t == endPoint) {
			StartCoroutine(MoveToPos(startPoint));
		}
	}

	void OnCollisionEnter(Collision c) {
		if (c.rigidbody == _rb) {
			_playerOnPlatform = true;
		}
	}

	void OnCollisionExit(Collision c) {
		if (c.rigidbody == _rb) {
			_playerOnPlatform = false;
		}
	}
}
