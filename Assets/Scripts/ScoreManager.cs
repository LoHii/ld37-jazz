﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ScoreManager : MonoBehaviour {

    static ScoreManager _sm;
    int totalScore;
    public Text scoreText;

    void Awake() {
        _sm = this;
    }

    public static void AddScore(int score) {
        if (_sm == null)
            return;

        _sm.totalScore += score;
        if (_sm.scoreText != null)
            _sm.scoreText.text = "" + _sm.totalScore;
    }
}
