﻿using UnityEngine;
using System.Collections;

public class BackwallMover : MonoBehaviour {

	public float moveAtDist;
	public float speed;
    public float[] positions;
	public AnimationCurve curve;
	Transform _player;
	AudioSource _moveSound;
    public float waitTimer;
    public Material activeMat;
    public AudioSource music;

    BoxCollider _coll;
    int _index;
	bool _running;

	void Start() {
		_player = GameManager.GetPlayer().transform;
		_moveSound = GameObject.Find("Door").GetComponent<AudioSource>();
        _coll = transform.GetChild(0).GetComponent<BoxCollider>();
    }

	void Update() {
        if (waitTimer > 0) {
            if (MenuManager.MenuExit())
                waitTimer -= Time.deltaTime;
            if (waitTimer <= 0) {
                transform.GetChild(0).GetComponent<MeshRenderer>().material = activeMat;
                _coll.enabled = false;
                GetComponent<AudioSource>().Play();
            }
        } else {
            var dist = transform.position.z - _player.position.z;
            if (dist < moveAtDist && !_running && _player.position.y >= -5f) {
                StartCoroutine(MoveWall());
            }
        }
	}

	IEnumerator MoveWall () {
		_running = true;
		_moveSound.Play();
        SpawnManager.NewCheckpoint();
		var startPos = transform.position;
        if (_index >= positions.Length) {
            StartCoroutine(End());
            yield break;
        }
        var endPos = Vector3.forward*positions[_index];

		var dist = endPos.z-startPos.z;
        var endDist = dist;

		while(dist > 0f) {
			var t = 1f - dist / endDist;;
			dist -= Time.deltaTime * speed;
			transform.position = Vector3.Lerp(startPos, endPos, curve.Evaluate(t));
			yield return new WaitForEndOfFrame();
		}
        transform.position = endPos;
		_running = false;
        _index++;
	}

    IEnumerator End() {
        InputManager.EndInputs();
        Quaternion rot = transform.rotation * Quaternion.Euler(new Vector3(0, 0, 180));
        Quaternion beginning = transform.rotation;
        transform.GetChild(0).GetComponent<BoxCollider>().enabled = true;
        float t = 1;

        while (t > 0) {
            _player.GetComponent<Rigidbody>().velocity = Vector3.zero;
            transform.rotation = Quaternion.Lerp(rot, beginning, t);
            t -= Time.deltaTime / 3;
            music.volume = 0.3f * t;
            yield return new WaitForEndOfFrame();
        }

        transform.rotation = rot;
        yield return new WaitForSeconds(1);
        MenuManager.End();
    }
}
