﻿using UnityEngine;
using System.Collections;

public class Hazard : MonoBehaviour {

	SpawnManager _sm;

	void Start() {
		_sm = FindObjectOfType<SpawnManager>();
	}

	void OnCollisionEnter(Collision c) {
		if (c.gameObject == GameManager.GetPlayer()) {
			_sm.Spawn();
			print("Player hit a hazard");
		}
	}
}
