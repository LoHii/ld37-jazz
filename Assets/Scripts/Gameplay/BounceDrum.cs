﻿using UnityEngine;
using System.Collections;

public class BounceDrum : MonoBehaviour {

    public float bigBounce;
    public float smallBounce;
    public GameObject burst;
    public float respawnTime;
    Vector3 _scale;
    bool _full =true;

    void Awake() {
        _scale = transform.localScale;
    }

    void Update() {
        if (!_full) {
            transform.localScale = Vector3.MoveTowards(transform.localScale, _scale, 2 * Time.deltaTime);
            if (transform.localScale == _scale)
                _full = true;
        }
    }

    void OnTriggerEnter(Collider c) {
        PlayerMover pm = c.GetComponent<PlayerMover>();

        if (pm != null && pm.transform.position.y > transform.position.y) {
            pm.Bounce(this);
        }
    }

    public void Break() {
        GameObject b = Instantiate(burst);
        b.transform.position = transform.position;
        Destroy(b, 5.0f);
        DrumRespawner dr = new GameObject().AddComponent<DrumRespawner>();
        dr.SetValues(this.gameObject, respawnTime);
        transform.localScale = Vector3.zero;
        _full = false;
        gameObject.SetActive(false);
    }
}
