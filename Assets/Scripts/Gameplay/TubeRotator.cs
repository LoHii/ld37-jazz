﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TubeRotator : InputReceiver {
	enum RotationDir { Left, Right };

	public float rotationSpeed;
	public LayerMask mask;

    public bool canJazz;
	bool running;
	Rigidbody rb;
    bool canRotate;
	bool rotationFlipped;
	AudioSource turnSound;
	AudioSource jazzHands;

	void Start() {
		rb = GameManager.GetPlayer().GetComponent<Rigidbody>();
		turnSound = GameObject.Find("TubeTurn").GetComponent<AudioSource>();
		jazzHands= GameObject.Find("JazzHands").GetComponent<AudioSource>();
	}

	public override void ReceiveInput (InputStruct input) {
        if (!canJazz)
            return;

		if (input.turnRight && !running && canRotate) {
			StartCoroutine(RotateTube(RotationDir.Right));
		}
		if (input.turnLeft && !running && canRotate) {
			StartCoroutine(RotateTube(RotationDir.Left));
		}
	}

	IEnumerator RotateTube (RotationDir dir) {
		turnSound.Play();
		jazzHands.Play();
		running = true;
        canRotate = false;
		var finalRot = new Quaternion();
		if (dir == RotationDir.Left) {
			var rot = rotationFlipped ? -90 : 90;
			finalRot = transform.rotation * Quaternion.Euler(0, 0, rot);
		} else {
			var rot = rotationFlipped ? 90 : -90;
			finalRot = transform.rotation * Quaternion.Euler(0, 0, rot);
		}

		var playerRot = transform.rotation;

        HornFader.Fade(true);
        Vector3 velocity = rb.velocity;
        //var playerPosAfter = PlayerPosAfterRotation(dir, finalRot);
        //rb.transform.position = playerPosAfter;
        //rb.isKinematic = true;
        PlayerMover pm = rb.GetComponent<PlayerMover>();
        pm.StartCoroutine(pm.Rotate(1 / rotationSpeed));

        InputManager.SendPlayerInputs(false);
		float t = 0f;

        while (t < 1f) {
			rb.velocity = Vector3.zero;
			transform.rotation = Quaternion.Lerp(playerRot, finalRot, t);
			t += Time.deltaTime * rotationSpeed;
			yield return new WaitForEndOfFrame();
		}
		transform.rotation = finalRot;

        rb.velocity = velocity;
        InputManager.SendPlayerInputs(true);
        //rb.isKinematic = false;
        running = false;
	}

    public void ResetCooldown() {
        if (canRotate == false)
            HornFader.Fade(false);
        canRotate = true;
    }

	public void FlipRotation () {
		rotationFlipped = !rotationFlipped;
	}

	/*
	// WIP
	Vector3 PlayerPosAfterRotation (RotationDir dir, Quaternion finalRot) {

		var castDir = new Vector3();
		var pos = rb.position;
		var posAfterRot = new Vector3();

		if (dir == RotationDir.Left) {
			castDir = Vector3.left;
			posAfterRot = new Vector3(pos.y, -pos.x, pos.z);
		} else {
			castDir = Vector3.right;
			posAfterRot = new Vector3(-pos.y, pos.x, pos.z);
		}

		return pos;
	}
	*/
}
