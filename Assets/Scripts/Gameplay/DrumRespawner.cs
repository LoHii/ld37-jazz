﻿using UnityEngine;
using System.Collections;

public class DrumRespawner : MonoBehaviour {

    GameObject _drum;
    float _timer;
    
	void Update () {
        _timer -= Time.deltaTime;

        if (_timer <= 0) {
            _drum.SetActive(true);
            Destroy(this.gameObject);
        }
	}

    public void SetValues(GameObject go, float timer) {
        _drum = go;
        _timer = timer;
    }
}
