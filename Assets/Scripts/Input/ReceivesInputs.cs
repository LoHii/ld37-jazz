﻿using UnityEngine;
using System.Collections;

public struct InputStruct {
    public Vector2 direction;
    public bool turnLeft;
    public bool turnRight;
    public bool pause;
    public bool jump;
    public bool holdJump;
}

public interface ReceivesInputs{

    void ReceiveInput(InputStruct input);
}
