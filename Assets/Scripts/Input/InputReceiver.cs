﻿using UnityEngine;
using System.Collections;
using System;

public abstract class InputReceiver : MonoBehaviour, ReceivesInputs {
    public virtual void ReceiveInput(InputStruct input) {
    }
}
