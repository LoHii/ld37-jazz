﻿using UnityEngine;
using System.Collections;

public class JazzHander : MonoBehaviour {

    public GameObject particle;

	void OnTriggerEnter(Collider c) {
        if (c.GetComponent<PlayerMover>() != null)
            GameManager.GetTube().GetComponent<TubeRotator>().canJazz = true;

        GameObject go = Instantiate(particle);
        Destroy(this.gameObject);
        go.transform.position = transform.position;
        Destroy(go, 5.0f);
    }
}
