﻿using UnityEngine;
using System.Collections;

public class InputManager : MonoBehaviour {

    static InputManager _im;
    InputReceiver _tube;
    InputReceiver _player;
    //public InputReceiver menu;
    bool _sendPlayer = true;

    void Awake() {
        _im = this;
    }

    void Start() {
        _player = GameManager.GetPlayer().GetComponent<InputReceiver>();
        GameObject tube = GameManager.GetTube();
        if (tube != null)
            _tube = tube.GetComponent<InputReceiver>();
    }

	void Update () {
        InputStruct input = CraftInput();

        if (_sendPlayer) { 
            _player.ReceiveInput(input);
        if (_tube != null)
            _tube.ReceiveInput(input);}
	}

    InputStruct CraftInput() {
        InputStruct i = new InputStruct();
        i.direction = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
        i.jump = Input.GetButtonDown("Jump");
        i.holdJump = Input.GetButton("Jump");
        i.turnRight = Input.GetButtonDown("Turn right");
        i.turnLeft = Input.GetButtonDown("Turn left");

        return i;
    }

    public static void EndInputs() {
        _im._player.ReceiveInput(new InputStruct());
        Destroy(_im);
    }

    static public void SendPlayerInputs(bool b) {
        _im._sendPlayer = b;
    }
}