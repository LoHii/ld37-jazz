﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using System.Collections;

public class SpawnManager : MonoBehaviour {

    List<GameObject> _reactivables = new List<GameObject>();
	public Transform[] checkPoints;
    static SpawnManager _sm;
	public bool reloadScene;
	public Transform spawnPoint;
	public Transform fellPastPoint;
    public AudioSource ded;

    public int maxScore;
    int _score = 0;
    int _index;
	Transform _player;
	Transform _tube;
	Rigidbody _playerRB;
	MenuManager _mm;
    Quaternion _rotation;
	Vector3[] _checkPointPositions;
    bool _spawning;

	void Awake() {
        _sm = this;
    }

	void Start () {
		_player = GameManager.GetPlayer().transform;
		_tube = GameManager.GetTube().transform;
		_playerRB = _player.GetComponent<Rigidbody>();
		_mm = GetComponent<MenuManager>();
		SetCheckpointPositions();
	}

	void Update () {
		if (_player.position.y < fellPastPoint.position.y) {
			Spawn();
		}

	}

	public void Spawn () {
		if (reloadScene) {
			SceneManager.LoadScene(SceneManager.GetActiveScene().name);
		} else {
            if (!_spawning)
                StartCoroutine(SpawnRoutine());
		}
		_mm.UnPause();
	}

    public static void AddToList(GameObject go) {
        if (_sm == null)
            return;

        _sm._reactivables.Add(go);
        go.SetActive(false);
    }

	void SetCheckpointPositions () {
		_checkPointPositions = new Vector3[checkPoints.Length];
		for (int i = 0; i < checkPoints.Length; i++) {
			_checkPointPositions[i] = checkPoints[i].position;
		}
	}

    public static void NewCheckpoint() {
        if (_sm == null)
            return;

        _sm._score += _sm._reactivables.Count;
        _sm._reactivables.Clear();
        _sm._rotation = _sm._player.GetComponent<PlayerMover>().lastRotation;
        _sm._index++;
        if (_sm._index < _sm._checkPointPositions.Length)
            _sm.spawnPoint.position = _sm._checkPointPositions[_sm._index];

    }

    public static void AddMax() {
        _sm.maxScore++;
    }

    public static int GetMax() {
        return _sm.maxScore;
    }
    public static int GetScore() {
        return _sm._score;
    }

    IEnumerator SpawnRoutine() {
        ded.Play();
        _spawning = true;
        _playerRB.velocity = Vector3.zero;
        yield return new WaitForSeconds(0.5f);

        foreach (GameObject go in _reactivables)
            go.SetActive(true);

        _reactivables.Clear();
        _playerRB.MovePosition(spawnPoint.position);
        _playerRB.velocity = Vector3.zero;
        _tube.rotation = _rotation;
        yield return new WaitForSeconds(0.3f);
        _spawning = false;
    }
}
