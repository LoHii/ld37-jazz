﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SnapPlayer : MonoBehaviour {
	public float bpm;
	
	public Vector2 pitchRange;

	AudioSource[] snaps;

	Transform _player;
	Rigidbody _rb;
	PlayerMover _pm;

	float _snapTimer;
	float interval;

	void Start() {
		snaps = transform.GetComponentsInChildren<AudioSource>();

		_player = GameManager.GetPlayer().transform;
		_rb = _player.GetComponent<Rigidbody>();
		_pm = _player.GetComponent<PlayerMover>();
        _snapTimer = 0;
	}

	void Update () {

		_snapTimer += Time.fixedDeltaTime;
		interval = 1f / bpm * 60f;
		if (_snapTimer >= interval) {
			if (_rb.velocity != Vector3.zero && _pm.IsGrounded() && !MenuManager.IsPaused()) {
				int n = Random.Range(0, snaps.Length);
				snaps[n].pitch = Random.Range(pitchRange.x, pitchRange.y);
				snaps[n].Play();
			}
			_snapTimer -= interval;
		}
	}
}
