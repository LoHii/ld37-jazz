﻿using UnityEngine;
using System.Collections;

public class Collectable : MonoBehaviour {
    
	AudioSource note;

	void Start() {
        SpawnManager.AddMax();
		note = GameObject.Find("Note Sound").GetComponent<AudioSource>();
	}

	void Update() {
		transform.Rotate(new Vector3(0, 0, 50) * Time.deltaTime);
	}

	void OnTriggerEnter(Collider Player) {
		SpawnManager.AddToList(gameObject);
		note.Play();
	}
}
