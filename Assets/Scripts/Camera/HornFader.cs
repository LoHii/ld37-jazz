﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HornFader : MonoBehaviour {

    static HornFader _hf;
    Image[] _horns;
    bool _visible;
    public float duration;
    public float maxAlpha;


    void Awake() {
        _hf = this;
        _horns = GetComponentsInChildren<Image>();
    }

    void Update() {

        if (!_visible) {
            foreach (Image im in _hf._horns)
                im.color = new Color(im.color.r, im.color.g, im.color.b, Mathf.Max(0, im.color.a - Time.deltaTime / (maxAlpha * duration)));
        } else {
            foreach (Image im in _hf._horns)
                im.color = new Color(im.color.r, im.color.g, im.color.b, Mathf.Min(maxAlpha, im.color.a + Time.deltaTime / (maxAlpha * duration)));
        }
    }

    public static void Fade(bool b) {
        if (_hf == null)
            return;

        _hf._visible = b;
    }
}
