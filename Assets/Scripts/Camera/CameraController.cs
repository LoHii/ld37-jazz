﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour {

    static CameraController _cc;
    Camera _cam;
    Transform _player;
    Vector3 _diff;
    //Transform _shaker;

    void Awake() {
        _cc = this;
        _cam = transform.GetChild(0).GetComponent<Camera>();
    }

	void Start () {
        _player = GameManager.GetPlayer().transform;
        _diff = transform.position - Vector3.forward * _player.position.z;
        //_shaker = transform.GetChild(0);
	}
	
	void LateUpdate () {
        transform.position = Vector3.forward * _player.position.z + _diff;
	}

    public static Camera GetCamera() {
        return _cc._cam;
    }
}
