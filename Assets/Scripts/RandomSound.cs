﻿using UnityEngine;
using System.Collections;

public class RandomSound : MonoBehaviour {

	public Vector2 pitchRange;
	AudioSource[] _sounds;

	void Start() {
		_sounds = transform.GetComponentsInChildren<AudioSource>();
	}

	public void PlaySound() {
		int ind = Random.Range(0, _sounds.Length);
		_sounds[ind].pitch = Random.Range(pitchRange.x, pitchRange.y);
		_sounds[ind].Play();
	}
}
